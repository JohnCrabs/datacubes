from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.readlines()

setup(
    name='datacubes',
    version='0.1',
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    extras_require=dict(tests=['pytest']),
    url='',
    author='ikavouras',
    author_email='ikavouras@mail.ntua.gr',
    description='',
    install_requires=requirements
)
