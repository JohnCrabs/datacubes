import cv2
import numpy as np
import csv
from PIL import Image
import rasterio as rs
import cv2 as cv

import os


IMAGE_EXT = ['png', 'jpeg', 'bmp']
VIDEO_EXT = ['avi', 'mp4', 'wmv']
LANDSAT_EXT = ['geotiff']


def readSimpleImage(path: str):
    if os.path.exists(path) and \
            os.path.isfile(path) and \
            os.path.basename(path).split('.')[1].lower() in IMAGE_EXT:
        return np.array(Image.open(path))

    return np.array([])


def readVideoFiles(path: str):
    if os.path.exists(path) and \
            os.path.isfile(path) and \
            os.path.basename(path).split('.')[1].lower() in VIDEO_EXT:
        frames = []
        video = cv2.VideoCapture(path)
        ret = True
        while ret:
            ret, frame = video.read()
            if ret:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                frames.append(frame)

        return np.array(frames), video.get(cv2.CAP_PROP_FPS)

    return np.array([]), 0
