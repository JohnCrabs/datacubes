import os
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog

from PIL import Image, ImageTk

FONT_LABEL_TAB = "Helvetica 10 bold"
FONT_LABEL_FRAME = "Helvetica 9 bold"
FONT_TEXT = "Helvetica 9"

ICON_SIZE_TAB_BAR = (16, 16)

LOGO_PATH = "icon/datacube_logo.png"

ICON_FOLDER = Image.open("icon/tabs/folder.png")
ICON_FOLDER.thumbnail(ICON_SIZE_TAB_BAR)

ICON_ARCHIVE = Image.open("icon/tabs/archive.png")
ICON_ARCHIVE.thumbnail(ICON_SIZE_TAB_BAR)

ICON_SATELLITE = Image.open("icon/tabs/satellite.png")
ICON_SATELLITE.thumbnail(ICON_SIZE_TAB_BAR)

ICON_SATELLITE_ANTENNA = Image.open("icon/tabs/satellite_antenna.png")
ICON_SATELLITE_ANTENNA.thumbnail(ICON_SIZE_TAB_BAR)

ICON_SPATIAL = Image.open("icon/tabs/spatial.png")
ICON_SPATIAL.thumbnail(ICON_SIZE_TAB_BAR)

ICON_IMAGE = Image.open("icon/tabs/image.png")
ICON_IMAGE.thumbnail(ICON_SIZE_TAB_BAR)

ICON_VIDEO = Image.open("icon/tabs/video.png")
ICON_VIDEO.thumbnail(ICON_SIZE_TAB_BAR)

ICON_CLIMATE = Image.open("icon/tabs/climate.png")
ICON_CLIMATE.thumbnail(ICON_SIZE_TAB_BAR)


class MainWindow:
    def __init__(self):
        # Create the main window
        self.rWin = tk.Tk()
        self.setRootWin(
            title="Data-Cubes Generator",
            resizable=True,
            iconPath=LOGO_PATH
        )
        self.rWin.geometry("1024x512")

        # ---------- Generate Icons ----------
        self._icon_folder = ImageTk.PhotoImage(ICON_FOLDER)
        self._icon_archive = ImageTk.PhotoImage(ICON_ARCHIVE)
        self._icon_satellite = ImageTk.PhotoImage(ICON_SATELLITE)
        self._icon_sat_antenna = ImageTk.PhotoImage(ICON_SATELLITE_ANTENNA)
        self._icon_spatial = ImageTk.PhotoImage(ICON_SPATIAL)
        self._icon_image = ImageTk.PhotoImage(ICON_IMAGE)
        self._icon_video = ImageTk.PhotoImage(ICON_VIDEO)
        self._icon_climate = ImageTk.PhotoImage(ICON_CLIMATE)

        # ---------- VARIABLES ----------

        # PATHS
        self.tvar_import_Landsat = tk.StringVar()
        self.tvar_import_Sentinel = tk.StringVar()

        # ------ START BUILDING THE APP ------

        # ======================================== #
        # Create a ttk.Notebook (Tab Widget) for
        # the different datacube methods
        # ======================================== #
        # START ---> ttk.Notebook ---> TabControl
        nb_tabGeneral = ttk.Notebook(self.rWin)

        # Create the tabs as frames
        f_tab_Auto = tk.Frame(nb_tabGeneral)
        f_tab_Satellite = tk.Frame(nb_tabGeneral)
        f_tab_Spatial = tk.Frame(nb_tabGeneral)
        f_tab_Image = tk.Frame(nb_tabGeneral)
        f_tab_Video = tk.Frame(nb_tabGeneral)
        f_tab_Climate = tk.Frame(nb_tabGeneral)

        # Add the tabs to the ttk.Notebook
        nb_tabGeneral.add(f_tab_Auto, text="From Directory",
                          image=self._icon_folder, compound=tk.LEFT)
        nb_tabGeneral.add(f_tab_Satellite, text="From Satellite Data",
                          image=self._icon_satellite, compound=tk.LEFT)
        nb_tabGeneral.add(f_tab_Spatial, text="From Geo-Spatial Data",
                          image=self._icon_spatial, compound=tk.LEFT)
        nb_tabGeneral.add(f_tab_Image, text="From Image Data",
                          image=self._icon_image, compound=tk.LEFT)
        nb_tabGeneral.add(f_tab_Video, text="From Video Data",
                          image=self._icon_video, compound=tk.LEFT)
        nb_tabGeneral.add(f_tab_Climate, text="From Climate Data",
                          image=self._icon_climate, compound=tk.LEFT)

        nb_tabGeneral.pack(side="left", anchor='n', expand=True, fill='both')
        # END ---> ttk.Notebook ---> TabControl

        # ---------------------
        # TAB - DIRECTORY
        # ---------------------

        # ---------------------
        # TAB - SATELLITE
        # ---------------------

        nb_tabSatellite = ttk.Notebook(f_tab_Satellite)

        f_tabLandsat = tk.Frame(nb_tabSatellite)
        f_tabSentinel = tk.Frame(nb_tabSatellite)

        nb_tabSatellite.add(f_tabLandsat, text="Landsat-Family",
                            image=self._icon_sat_antenna, compound=tk.LEFT)
        nb_tabSatellite.add(f_tabSentinel, text="Sentinel-Family",
                            image=self._icon_sat_antenna, compound=tk.LEFT)
        nb_tabSatellite.pack(side="left", anchor='n', expand=True, fill='both')

        # ---------------------
        # TAB - LANDSAT
        # ---------------------
        # Add TOP Frame - Import Data
        lf_tabLandsat_Browse = tk.LabelFrame(f_tabLandsat, text="Import Data")

        f_landsatBrowse_Buttons = tk.Frame(lf_tabLandsat_Browse)
        # Label
        tk.Label(f_landsatBrowse_Buttons, text="Browse: ").pack(side="left", anchor="n", pady=3)
        # Browse Line Text Bar
        tk.Entry(f_landsatBrowse_Buttons, textvariable=self.tvar_import_Landsat,
                 state="readonly").pack(side="left", anchor='n', expand=True, fill='x', pady=5)
        # Browse Archive
        tk.Button(f_landsatBrowse_Buttons, text="Browse Archive", image=self._icon_archive, compound="left",
                  height=20, command=self.browseArchive_Landsat).pack(side="left", anchor="n")
        # Browse Directory
        tk.Button(f_landsatBrowse_Buttons, text="Browse Directory", image=self._icon_folder,  compound="left",
                  height=20, command=self.browseDir_Landsat).pack(side="left", anchor="n")
        f_landsatBrowse_Buttons.pack(side="top", anchor='n', pady=2, expand=True, fill='x')

        lf_tabLandsat_Browse.pack(side="left", anchor='n', expand=True, fill='x')

        # Add TOP Frame - Import Data

        # ---------------------
        # TAB - SENTINEL
        # ---------------------
        # Add TOP Frame - Import Data
        lf_tabSentinel_Browse = tk.LabelFrame(f_tabSentinel, text="Import Data")

        f_sentinelBrowse_Buttons = tk.Frame(lf_tabSentinel_Browse)
        # Label
        tk.Label(f_sentinelBrowse_Buttons, text="Browse: ").pack(side="left", anchor="n", pady=3)
        # Browse Line Text Bar
        tk.Entry(f_sentinelBrowse_Buttons, textvariable=self.tvar_import_Sentinel,
                 state="readonly").pack(side="left", anchor='n', expand=True, fill='x', pady=5)
        # Browse Archive
        tk.Button(f_sentinelBrowse_Buttons, text="Browse Archive", image=self._icon_archive, compound="left",
                  height=20, command=self.browseArchive_Sentinel).pack(side="left", anchor="n")
        # Browse Directory
        tk.Button(f_sentinelBrowse_Buttons, text="Browse Directory", image=self._icon_folder, compound="left",
                  height=20, command=self.browseDir_Sentinel).pack(side="left", anchor="n")
        f_sentinelBrowse_Buttons.pack(side="top", anchor='n', pady=2, expand=True, fill='x')

        lf_tabSentinel_Browse.pack(side="left", anchor='n', expand=True, fill='x')

        # ---------------------
        # TAB - GEO-SPATIAL
        # ---------------------

        # ---------------------
        # TAB - IMAGE
        # ---------------------

        # ---------------------
        # TAB - VIDEO
        # ---------------------

        # ---------------------
        # TAB - CLIMATE
        # ---------------------

        # ======================================== #
        # ======================================== #

        # ------ END OF CODE SECTION / START EXEC LOOP ------
        # The MainLoop
        self.rWin.mainloop()

    def setRootWin(self, title: str, resizable: bool, iconPath: str):
        self.rWin.title(title)
        try:
            self.rWin.iconphoto(False, tk.PhotoImage(file=iconPath))
        except:
            pass
        self.rWin.resizable(resizable, resizable)

    # ---------- #
    #   BROWSE   #
    # ---------- #
    @staticmethod
    def browseDir():
        pathDir = filedialog.askdirectory()
        if os.path.exists(pathDir):
            return pathDir
        return None

    @staticmethod
    def browseFile(filetypes=()):
        pathFile = filedialog.askopenfilename(
            filetypes=filetypes
        )
        if os.path.exists(pathFile):
            return pathFile
        return None

    # Browse Path --> LANDSAT
    def browseDir_Landsat(self):
        inDir = self.browseDir()
        if inDir:
            self.tvar_import_Landsat.set(inDir)

    def browseArchive_Landsat(self):
        inPath = self.browseFile(
            filetypes=(
                ("Archived File", ("*.zip", "*.tar", "*.tar.gz")),
                ("All Files", "*.*"),
                )
        )
        if inPath:
            self.tvar_import_Landsat.set(inPath)

    # Browse Path --> SENTINEL
    def browseDir_Sentinel(self):
        inDir = self.browseDir()
        if inDir:
            self.tvar_import_Sentinel.set(inDir)

    def browseArchive_Sentinel(self):
        inPath = self.browseFile(
            filetypes=(
                ("Archived File", ("*.zip", "*.tar", "*.tar.gz")),
                ("All Files", "*.*"),
                )
        )
        if inPath:
            self.tvar_import_Sentinel.set(inPath)


if __name__ == "__main__":
    MainWindow()
